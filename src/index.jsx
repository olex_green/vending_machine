import React from 'react';
import ReactDOM from 'react-dom';

import { MuiThemeProvider } from '@material-ui/core/styles';

import Machine from './components/MachineContainer';

ReactDOM.render(
  (
    <MuiThemeProvider>
      <Machine />
    </MuiThemeProvider>
  ), document.getElementById('app'),
);
