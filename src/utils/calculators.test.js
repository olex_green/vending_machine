import { expect } from 'chai';

import { calculateChange, makeResultText } from './calculators';

describe('utils::calculators', () => {
  describe('calculateChange', () => {
    it('pass empty parameters. Should not throw error', () => {
      expect(() => {
        calculateChange([], {});
      }).to.not.throw();
    });

    it('pass empty enteredCoins and Selection. Should return 0', () => {
      const expected = 0;
      expect(
        calculateChange([], { label: 'Hazelnut', value: 3.1 }),
      ).to.equal(expected);
    });

    it('pass single enteredCoin and empty Selection. Should return 0', () => {
      const expected = 0;
      expect(
        calculateChange(
          [{
            label: '20c',
            value: 0.2,
            isValid: true,
          }],
          {},
        ),
      ).to.equal(expected);
    });

    it('pass 5c enteredCoin and Selection. Should return 5c', () => {
      const expected = 0;
      expect(
        calculateChange(
          [{
            label: '5c',
            value: 0.05,
            isValid: false,
          }],
          {
            label: 'Hazelnut',
            value: 3.1,
          },
        ),
      ).to.equal(expected);
    });

    it('pass 5c + $2 enteredCoin and Selection = $2. Should return 5c', () => {
      const expected = 0;
      expect(
        calculateChange(
          [
            {
              label: '5c',
              value: 0.05,
              isValid: false,
            },
            {
              label: '$2',
              value: 2,
              isValid: true,
            },
          ],
          {
            label: 'Organic Raw',
            value: 2,
          },
        ),
      ).to.equal(expected);
    });

    it('pass 20c + $2 enteredCoin and Selection = $2. Should return 20c', () => {
      const expected = 0;
      expect(
        calculateChange(
          [
            {
              label: '20c',
              value: 0.2,
              isValid: true,
            },
            {
              label: '$2',
              value: 2,
              isValid: true,
            },
          ],
          {
            label: 'Organic Raw',
            value: 2,
          },
        ),
      ).to.equal(expected);
    });

    it('pass 20c + $2 enteredCoin and Selection = 3.1. Should return 20c', () => {
      const expected = 0;
      expect(
        calculateChange(
          [
            {
              label: '20c',
              value: 0.2,
              isValid: true,
            },
            {
              label: '$2',
              value: 2,
              isValid: true,
            },
          ],
          {
            label: 'Hazelnut',
            value: 3.1,
          },
        ),
      ).to.equal(expected);
    });
  });

  describe('makeResultText', () => {
    it('pass empty parameters. Should not throw error', () => {
      expect(() => {
        makeResultText([], {});
      }).to.not.throw();
    });

    it('pass empty enteredCoins and Selection. Should return - Please enter coins', () => {
      const expected = 'Please enter coins';
      expect(
        calculateChange([], { label: 'Hazelnut', value: 3.1 }),
      ).to.equal(expected);
    });

    it('pass single enteredCoin and empty Selection. Should return - Please select a chocolate bar', () => {
      const expected = 'Please select a chocolate bar';
      expect(
        calculateChange(
          [{
            label: '20c',
            value: 0.2,
            isValid: true,
          }],
          {},
        ),
      ).to.equal(expected);
    });

    it('pass 5c enteredCoin and Selection. Should return - 5c is not valid, please enter 10c, 20c, 50c, $1, $2', () => {
      const expected = '5c is not valid, please enter 10c, 20c, 50c, $1, $2';
      expect(
        calculateChange(
          [{
            label: '5c',
            value: 0.05,
            isValid: false,
          }],
          {
            label: 'Hazelnut',
            value: 3.1,
          },
        ),
      ).to.equal(expected);
    });

    it('pass 5c + $2 enteredCoin and Selection = $2. Should return - Take your change', () => {
      const expected = 'Take your change';
      expect(
        calculateChange(
          [
            {
              label: '5c',
              value: 0.05,
              isValid: false,
            },
            {
              label: '$2',
              value: 2,
              isValid: true,
            },
          ],
          {
            label: 'Organic Raw',
            value: 2,
          },
        ),
      ).to.equal(expected);
    });

    it('pass 20c + $2 enteredCoin and Selection = $2. Should return - Take your change', () => {
      const expected = 'Take your change';
      expect(
        calculateChange(
          [
            {
              label: '20c',
              value: 0.2,
              isValid: true,
            },
            {
              label: '$2',
              value: 2,
              isValid: true,
            },
          ],
          {
            label: 'Organic Raw',
            value: 2,
          },
        ),
      ).to.equal(expected);
    });

    it('pass 20c + $2 enteredCoin and Selection = 3.1. Should return - Enter more coins', () => {
      const expected = 'Enter more coins';
      expect(
        calculateChange(
          [
            {
              label: '20c',
              value: 0.2,
              isValid: true,
            },
            {
              label: '$2',
              value: 2,
              isValid: true,
            },
          ],
          {
            label: 'Hazelnut',
            value: 3.1,
          },
        ),
      ).to.equal(expected);
    });
  });
});
