import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Machine from './Machine';
import MachineContainer from './MachineContainer';

const defaultProps = {
  classes: {},
};

function setup(props) {
  const mergedProps = {
    ...defaultProps,
    ...props,
  };

  const enzymeWrapper = shallow(
    <MachineContainer {...mergedProps} />,
  );

  return {
    enzymeWrapper,
  };
}

describe('components', () => {
  describe('MachineContainer', () => {
    describe('checking rendering', () => {
      it('should render self and has first div', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.first().is(Machine)).to.be.true;
      });
    });
  });
});
