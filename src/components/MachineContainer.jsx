import React from 'react';

import Machine from './Machine';
import { calculateChange, makeResultText } from '../utils/calculators';

const ALL_COINS = [
  {
    label: '5c',
    value: 0.05,
    isValid: false,
  },
  {
    label: '10c',
    value: 0.1,
    isValid: true,
  },
  {
    label: '20c',
    value: 0.2,
    isValid: true,
  },
  {
    label: '50c',
    value: 0.5,
    isValid: true,
  },
  {
    label: '$1',
    value: 1,
    isValid: true,
  },
  {
    label: '$2',
    value: 2,
    isValid: true,
  },
];

const SELECTIONS = [
  {
    label: 'Caramel',
    value: 2.5,
  },
  {
    label: 'Hazelnut',
    value: 3.1,
  },
  {
    label: 'Organic Raw',
    value: 2,
  },
];

class MachineContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      enteredCoins: [],
      changeCoins: [],
      resultText: '',
    };
  }

  onEnterCoin = (value) => {
    this.setState(prevState => ({
      enteredCoins: [...prevState.enteredCoins, value],
    }));
  }

  onSelect = (value) => {
    const { enteredCoins } = this.state;

    const changeCoins = calculateChange(enteredCoins, value);
    const resultText = makeResultText();

    this.setState({ changeCoins, resultText });
  }

  render() {
    const {
      changeCoins,
      enteredCoins,
      resultText,
    } = this.state;

    return (
      <Machine
        changeCoins={changeCoins}
        coins={ALL_COINS}
        enteredCoins={enteredCoins}
        onEnterCoin={this.onEnterCoin}
        onSelect={this.onSelect}
        resultText={resultText}
        selections={SELECTIONS}
      />
    );
  }
}

export default MachineContainer;
