import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Button from '@material-ui/core/Button';

import { Machine } from './Machine';

const defaultProps = {
  classes: {},
};

function setup(props) {
  const mergedProps = {
    ...defaultProps,
    ...props,
  };

  const enzymeWrapper = shallow(
    <Machine {...mergedProps} />,
  );

  return {
    enzymeWrapper,
  };
}

describe('components', () => {
  describe('Machine', () => {
    describe('checking rendering', () => {
      it('should render self and has first div', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.first().is('div')).to.be.true;
      });

      it('should not have any buttons by default', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find(Button).length).to.equal(0);
      });
    });
  });
});
