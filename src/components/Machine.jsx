import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';

const styles = () => ({
  boxRoot: {
    display: 'flex',
    '& button': {
      marginRight: 16,
    },
  },
  divider: {
    marginTop: 24,
  },
});

export function Machine({
  classes,
  coins,
  enteredCoins,
  changeCoins,
  selections,
  resultText,
  onSelect,
  onEnterCoin,
}) {
  return (
    <div>
      <h2>
        Vending Machine
      </h2>
      <h3>Enter Coins</h3>
      <div className={classes.boxRoot}>
        {
          coins.map((item) => {
            const onClick = () => {
              onEnterCoin(item);
            };

            return (
              <Button variant="fab" onClick={onClick}>
                {item.label}
              </Button>
            );
          })
        }
      </div>
      <hr className={classes.divider} />
      <h3>Entered Coins</h3>
      <div className={classes.boxRoot}>
        {
          enteredCoins.map(item => (
            <Button variant="fab">
              {item.label}
            </Button>
          ))
        }
      </div>
      <hr className={classes.divider} />
      <h3>Chocolate Bars</h3>
      <div className={classes.boxRoot}>
        {
          selections.map((item) => {
            const onClick = () => {
              onSelect(item);
            };

            return (
              <Button variant="extendedFab" onClick={onClick}>
                {item.label}
              </Button>
            );
          })
        }
      </div>
      <hr className={classes.divider} />
      <h3>Result</h3>
      {
        resultText
      }
      <hr className={classes.divider} />
      <h3>Your Change</h3>
      <div className={classes.boxRoot}>
        {
          changeCoins.map(item => (
            <Button variant="fab">
              {item.label}
            </Button>
          ))
        }
      </div>
    </div>
  );
}

Machine.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  coins: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number,
  })),
  enteredCoins: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number,
  })),
  selections: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number,
  })),
  changeCoins: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number,
  })),
  resultText: PropTypes.string,
  onSelect: PropTypes.func.isRequired,
  onEnterCoin: PropTypes.func.isRequired,
};

Machine.defaultProps = {
  coins: [],
  enteredCoins: [],
  selections: [],
  changeCoins: [],
  resultText: '',
};

export default withStyles(styles)(Machine);
