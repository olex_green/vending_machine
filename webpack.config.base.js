const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = function base(isDebug) {
  return {
    context: path.resolve(__dirname, './src'),
    output: {
      path: `${__dirname}/dist`,
      filename: isDebug ? 'bundle.js' : '[name].[hash].js',
      publicPath: '/',
    },
    target: 'web',
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: [
        path.join(__dirname, 'node_modules'),
        path.join(__dirname, 'src'),
      ],
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: [path.join(__dirname, 'node_modules')],
          loader: 'babel-loader',
          include: [path.join(__dirname, 'src')],
          options: {
            cacheDirectory: isDebug,
            babelrc: true,
          },
        },
        {
          test: /\.s?[ac]ss$/,
          exclude: [path.join(__dirname, 'node_modules')],
          use: [
            isDebug ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.s?[ac]ss$/,
          include: [path.join(__dirname, 'node_modules')],
          use: [
            'style-loader',
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          loader: 'file-loader',
          options: {
            name() {
              if (process.env.NODE_ENV === 'development') {
                return '[path][name].[ext]';
              }

              return '[hash].[ext]';
            },
            outputPath: 'images/',
          },
        },
        { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file' },
        { test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000' },
        {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&mimetype=application/octet-stream',
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new HtmlWebpackPlugin({
        template: 'index.ejs',
        meta: {
          release: process.env.TAG || process.env.BRANCH || '',
        },
        minify: {
          removeComments: true,
          collapseWhitespace: true,
        },
        inject: true,
      }),
      new MiniCssExtractPlugin({
        filename: isDebug ? '[name].css' : '[name].[hash].css',
        chunkFilename: isDebug ? '[id].css' : '[id].[hash].css',
      }),
    ],
  };
};
