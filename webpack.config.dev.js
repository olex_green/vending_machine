const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const path = require('path');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const commonConfig = require('./webpack.config.base.js');


module.exports = function dev() {
  return webpackMerge(commonConfig(true), {
    mode: 'development',
    entry: path.resolve(__dirname, './src/index.jsx'),
    devtool: 'cheap-module-eval-source-map',
    devServer: {
      historyApiFallback: true,
      clientLogLevel: 'warning',
      compress: true,
      contentBase: 'dist/',
      hot: false,
      inline: true,
      port: process.env.PORT || 3001,
      host: '0.0.0.0',
      disableHostCheck: true,
      quiet: false,
      noInfo: false,
    },
    optimization: {
      minimize: false,
    },
    watchOptions: {
      ignored: /node_modules/,
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
      new BundleAnalyzerPlugin({
        openAnalyzer: false,
        analyzerPort: 3034,
      }),
    ],
  });
};
